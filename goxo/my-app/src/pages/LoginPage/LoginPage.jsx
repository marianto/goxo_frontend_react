import React from 'react';
import './LoginPage.scss';
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';
import { API } from '../../shared/services/api';
import loginImg from '../../shared/assets/images/welcome-imgjpg.jpg'
export default function LoginPage() {
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = (data) => {
        API.post('users/login', data).then(res => {
            localStorage.setItem('token', res.data.token);
            if (res.data.logged) {
                window.location.href = "/menu"
            }
        })
            .catch((error) => {
                alert('Los campos no son correctos.')
            })
    };

    return (
        <div className="base-container">

            <div className="header">
                <h2>¡Gracias por registrarte!</h2>
                <p>Por favor, introduce tus datos para continuar</p>
            </div>
            <div className="content">
                <div className="image">
                    <img src={loginImg} alt="girl-login" />
                </div>
                <form onSubmit={handleSubmit(onSubmit)} className="form">

                    <div className="form-group">
                        <label htmlFor="email">
                            <input type="email" className="form-control mb-4" name="email" id="email"
                                placeholder="Dirección email"
                                ref={register({ required: true })}
                            />
                            {errors.email && <span className="form__span">El campo email es requerido</span>}

                        </label>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">
                            <input type="password" className="form-control mb-4" name="password"
                                id="password" placeholder="Contraseña" ref={register({ required: true })}
                            />
                            {errors.password && <span className="form__span">El campo password es requerido</span>}

                        </label>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary btn-block">Entrar</button>
                    </div>

                    <div className="footer">
                        <p className="footer__p">¿Áún no te has registrado?</p>
                        <Link to="/register" className="footer__link">Crea tu cuenta aquí</Link>
                    </div>
                </form>

            </div>
        </div>
    )
}