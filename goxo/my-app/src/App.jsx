import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import RegisterPage from './pages/RegisterPage/RegisterPage';
import LoginPage from './pages/LoginPage/LoginPage';
import UserPage from './pages/UserPage/UserPage';


function App() {
  return (
    <Router>
      <Switch>
      <Route path="/menu">
          <UserPage/>
        </Route>
       <Route path="/login">
         <LoginPage/>
       </Route>
        <Route>
        <RegisterPage path="/register" />
        </Route>
      
      </Switch>
    </Router>
  );
}

export default App;
