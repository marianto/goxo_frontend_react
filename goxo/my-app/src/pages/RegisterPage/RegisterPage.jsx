import React from 'react'
import './RegisterPage.scss';
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';
import { API } from '../../shared/services/api';
import registerImg from '../../shared/assets/images/logIn.jpg';

export default function RegisterPage() {
    const { register, handleSubmit, errors } = useForm();

    const onSubmit = (data) => {
        console.log(data);

        API.post('users', data, {

        }).then(res => {
            localStorage.setItem('token', res.data.token);
            window.location.href = "/menu";

        })
            .catch((error) => {
                alert('Los campos son incorrectos.')
            })
    };

    return (
        <div className="base-container">
            <div className="header">
                <h2>Dinos quién eres</h2>
            </div>
            <div className="content">
                <div className="image">
                    <img src={registerImg} alt="girl-login" />
                </div>
                <form onSubmit={handleSubmit(onSubmit)} className="form">
                    <div className="form-group">
                        <label htmlFor="name">
                            <input type="text" className="form-control mb-4" name="name" id="name"
                                placeholder="Nombre Completo"
                                ref={register({ required: true })}
                            />
                            {errors.name && <span className="form__span">El campo nombre es requerido</span>}

                        </label>
                    </div>
                    <div className="form-group">
                        <label htmlFor="city">
                            <input type="text" className="form-control mb-4" name="city" id="city"
                                placeholder="Ciudad"
                                ref={register({ required: false })}
                            />

                        </label>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">
                            <input type="email" className="form-control mb-4" name="email" id="email"
                                placeholder="Dirección email"
                                ref={register({ required: true })}
                            />
                            {errors.email && <span className="form__span">El campo email es requerido</span>}

                        </label>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">
                            <input type="password" className="form-control mb-4" name="password"
                                id="password" placeholder="Contraseña" ref={register({ required: true })}
                            />
                            {errors.password && <span className="form__span">El campo password es requerido</span>}

                        </label>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary btn-block">Guardar Perfil</button>
                    </div>

                    <div className="footer">
                        <p className="footer__p">¿Ya has iniciado sesión?</p>
                        <Link to="/login" className="footer__link">Inicia sesión aquí</Link>
                    </div>
                </form>

            </div>

        </div>
    )
}
