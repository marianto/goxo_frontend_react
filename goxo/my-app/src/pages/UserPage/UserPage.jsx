import React, { useState, useEffect } from 'react';
import './UserPage.scss'
import { API } from '../../shared/services/api';
import menuImg from '../../shared/assets/images/menu.jpg'
import { faMapMarkedAlt, faUser } from "@fortawesome/free-solid-svg-icons";
import { Link } from 'react-router-dom';
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function UserPage() {

    const [userData, setUserData] = useState('')

    useEffect(() => {

        API.get('users/singleUser').then(res => {
            const userData = res.data;
            console.log(userData);
            setUserData(userData);

        })
    }, [])

    const {name, email, city} = userData;

    return (

        <div className="base-container">
            <div className="header">
                <h2>¡Hola {name}!</h2>
            </div>
            <div className="content">
                <div className="image">
                    <img src={menuImg} alt="girls-enjoying" />
                </div>

                <form className="form">
                    <p>Comprueba que tus datos sean correctos</p>
                    <div className="form-group">
                        <label>
                            <FontAwesomeIcon icon={faUser} />
                            <p>{name}</p>
                        </label>
                    </div>
                    <div className="form-group">

                        <label>
                            <FontAwesomeIcon icon={faEnvelope} />
                            <p>{email}</p>
                        </label>
                    </div>
                    <div className="form-group">

                        <label>
                            <FontAwesomeIcon icon={faMapMarkedAlt} />
                            <p>{city}</p>
                        </label>
                    </div>



                    <div className="footer-menu">
                        <p className="footer-menu__p">¿Quieres volver atrás?</p>
                        <Link to="/register" className="footer-menu__link-icon">
                            <FontAwesomeIcon icon={faLongArrowAltLeft} />
                        </Link>
                    </div>



                </form>


            </div>







        </div>






    )
}